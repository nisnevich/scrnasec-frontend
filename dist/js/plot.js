var d3 = Plotly.d3;
var plotContainer = document.getElementById("plotContainer");

var plotHeight = 700;
var plotWidth = 900;
var featureSize = 1;

function get_text(size) {
  var arr = Array(size);
  for (i = 0; i < size; i++) {
    arr[i] = "text " + i;
  }
  return arr;
}

d3.tsv("data/data_plot.tsv",
  function(d) {
    return {
      x0:   +d.x1,
      y0:   +d.y1,
      x1:   +d.x2,
      y1:   +d.y2,
      x2:   +d.x3,
      y2:   +d.y3,
      someData:   +d.someData
    };
  },
  function(error, data) {

    let x0 = [];
    let x1 = [];
    let x2 = [];
    let y0 = [];
    let y1 = [];
    let y2 = [];
    let someData = [];

    for (i = 0; i < data.length; i++) {
      x0.push(data[i].x0);
      y0.push(data[i].y0);
      x1.push(data[i].x1);
      y1.push(data[i].y1);
      x2.push(data[i].x2);
      y2.push(data[i].y2);
      someData.push(data[i].someData);
    }

    var matrixData = [
      {
        type: "scattergl",
        x: x0,
        y: y0,

        hovertext: someData,
        hoverinfo: 'text+x+y',

        mode: 'markers',
        name: 'fish',
        marker: {size: featureSize, color:'blue'},
        legendgroup: 'a'
      }, {
        type: "scattergl",
        x: x1,
        y: y1,
        hovertext: someData,
        hoverinfo: 'text+x+y',
        mode: 'markers',
        name: 'mouse',
        marker: {size: featureSize, color:'red'},
        legendgroup: 'b'
      }, {
        type: "scattergl",
        x: x2,
        y: y2,
        hovertext: someData,
        hoverinfo: 'text+x+y',
        mode: 'markers',
        name: 'worm',
        marker: {size: featureSize, color:'green'},
        legendgroup: 'c'
      }
    ];

// https://plot.ly/javascript/reference/#layout
    var layout = {
      shapes: [
        {
          type: 'circle', // "circle" | "rect" | "path" | "line"
          xref: 'x',
          yref: 'y',
          x0: d3.min(x0),
          y0: d3.min(y0),
          x1: d3.max(x0),
          y1: d3.max(y0),
          opacity: 0.2,
          fillcolor: 'blue',
          line: {
            color: 'blue',
            dash: 'solid'
          },
          legendgroup: 'a'
        },
        {
          type: 'circle',
          xref: 'x',
          yref: 'y',
          x0: d3.min(x1),
          y0: d3.min(y1),
          x1: d3.max(x1),
          y1: d3.max(y1),
          opacity: 0.2,
          fillcolor: 'red',
          line: {
            color: 'red'
          },
          legendgroup: 'b'
        },
        {
          type: 'circle',
          xref: 'x',
          yref: 'y',
          x0: d3.min(x2),
          y0: d3.min(y2),
          x1: d3.max(x2),
          y1: d3.max(y2),
          opacity: 0.2,
          fillcolor: 'green',
          line: {
            color: 'green'
          },
          legendgroup: 'c'
        }
      ],
      height: plotHeight,
      width: plotWidth,
      showlegend: true,
      legend: {
        traceorder: 'grouped'
      },
      hovermode:'closest' // x, y
    };
    // https://plot.ly/javascript/configuration-options/
    Plotly.newPlot('plotContainer', matrixData, layout);

    plotContainer.on('plotly_afterplot', function(){
      $("#plotContainer").find(".svg-container").css("box-shadow", "0 0 5px white");
    });
  });
