jQuery(document).ready(function(){
  // var slider = new Cachu(document.querySelector('.cachu__container'),{
  //
  //   // disable mousewheel interactions
  //   disableMouseEvents: false,
  //
  //   // animation speed in milliseconds
  //   scrollingSpeed: 1000,
  //
  //   // infinite loop
  //   scrollingLoop: true,
  //
  //   // or 'horizontal'
  //   scrollingDirection: 'vertical',
  //
  //   // enable side navigation
  //   navigationEnabled: true,
  //
  //   // navigation position
  //   // or 'top', 'bottom', 'left'
  //   navigationPosition: 'right'
  //
  // });
  // slider.run();

  $(".mainContainer").onepage_scroll({
    sectionContainer: "section",
    responsiveFallback: false,
    loop: true
    // ,easing: "ease",                  // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in",
    // // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
    // animationTime: 1000,             // AnimationTime let you define how long each section takes to animate
    // pagination: true,                // You can either show or hide the pagination. Toggle true for show, false for hide.
    // updateURL: false,                // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
    // beforeMove: function(index) {},  // This option accepts a callback function. The function will be called before the page moves.
    // afterMove: function(index) {},   // This option accepts a callback function. The function will be called after the page moves.
    // keyboard: true,                  // You can activate the keyboard controls
    // direction: "vertical"            // You can now define the direction of the One Page Scroll animation. Options available are "vertical" and "horizontal". The default value is "vertical".
  });
});

// $(window).load(function() {
//     // When the page has loaded
//     $("body").fadeIn(1000);
// });
